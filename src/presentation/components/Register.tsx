import { FunctionComponent } from "react";
import {
  IonApp,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonCol,
  IonRow,
  IonIcon,
  IonItem,
  IonLabel,
  IonInput,
  IonButton,
  useIonAlert,
} from "@ionic/react";
import { logIn } from "ionicons/icons";
import { RegisterUseCase } from "../../domain/authentication/use-cases/register/register.use-case";
import { taskEither } from "fp-ts";
import { pipe } from "fp-ts/function";
import { toPromise } from "../../utils/functions";

export interface RegisterProps {
  email?: string;
  registerUseCase: RegisterUseCase;
}

export interface RegisterState {
  email: string;
  error?: Error;
}

const Register: FunctionComponent<RegisterProps> = (props) => {
  const state: RegisterState = {
    email: props.email ?? "",
  };
  const setEmail = (email: string): void => {
    state.email = email;
  };
  const [presentAlert] = useIonAlert();
  const register = async (): Promise<void> => {
    return pipe(
      props.registerUseCase.execute({
        email: state.email ?? "",
      }),
      taskEither.mapLeft((error) => {
        presentAlert({
          header: "Error",
          subHeader: "An error occurred",
          message: error.message,
          buttons: ["OK"],
        });
        return error;
      }),
      toPromise
    ).catch(() => Promise.resolve());
  };
  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Register</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonRow>
        <IonCol>
          <IonIcon
            style={{ fontSize: "70px", color: "#0040ff" }}
            icon={logIn}
          />
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonItem>
            <IonLabel position="floating">Email</IonLabel>
            <IonInput
              type="email"
              value={state.email}
              onIonChange={(e) => setEmail(e.detail.value!)}
            ></IonInput>
          </IonItem>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <p style={{ fontSize: "small" }}>
            By clicking on this button you agree to our{" "}
            <a href="http://www.google.com">Policy</a>
          </p>
          <IonButton expand="block" onClick={register}>
            Register
          </IonButton>
        </IonCol>
      </IonRow>
    </IonApp>
  );
};

export default Register;

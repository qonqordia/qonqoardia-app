import { option } from 'fp-ts';
import { Option } from 'fp-ts/Option';
import { Observable } from "../../utils/Observable";
import { Authenticator } from "../entities/authenticator";

export type AuthenticationState = {
  authenticator: Observable<Option<Authenticator>>;
};

export const authenticationState: AuthenticationState = {
  authenticator: new Observable<Option<Authenticator>>(option.none),
};

export class InternalError extends Error {
  public static readonly CODE = "INTERNAL";

  public originalMessage?: string;

  constructor(message?: string, options?: ErrorOptions) {
    super(InternalError.CODE, options);
    this.originalMessage = message;
  }
}

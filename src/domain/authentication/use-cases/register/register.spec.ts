import { option } from "fp-ts";
import { InternalError } from "../../../../common/errors/internal.error";
import { InMemoryAuthenticationRepository } from "../../../../infrastructure/in-memory/in-memory-authentication.repository";
import { toPromise } from "../../../../utils/functions";
import { EmailAlreadyUsedError } from "../../errors/email-already-used.error";
import { WrongCredentialsError } from "../../errors/wrong-credentials.error";
import { RegisterResult } from "./register.result";
import { RegisterUseCase } from "./register.use-case";

describe("register", () => {
  const email = "e2e075b5-c5ed-4456-a359-80971ffc481f@77745dfa-4dd1-432a-89e9-9e40138b0d89";

  let authenticationRepository: InMemoryAuthenticationRepository;
  let useCase: RegisterUseCase;

  let promise: Promise<RegisterResult>;

  describe("when api is up and running", () => {
    describe("when infos are correct", () => {
      beforeEach(() => {
        authenticationRepository = new InMemoryAuthenticationRepository();
        useCase = new RegisterUseCase(authenticationRepository);
        promise = toPromise(useCase.execute({ email }));
      });
      test("should register", async () => {
        await expect(promise).resolves.toBe(undefined);
      });
    });
  
    describe("when email has already been used", () => {
      beforeEach(() => {
        authenticationRepository = new InMemoryAuthenticationRepository([{
          email,
          password: option.none,
        }]);
        useCase = new RegisterUseCase(authenticationRepository);
        promise = toPromise(useCase.execute({ email }));
      });
      test("should handle error", async () => {
        await expect(promise).rejects.toThrow(EmailAlreadyUsedError.CODE);
      });
    });
  });

  describe("when api is down", () => {
    beforeEach(() => {
      authenticationRepository = new InMemoryAuthenticationRepository([], true);
      useCase = new RegisterUseCase(authenticationRepository);
      promise = toPromise(useCase.execute({ email }));
    });
    test("should handle error", async () => {
      await expect(promise).rejects.toThrow(InternalError.CODE);
    });
  });
});
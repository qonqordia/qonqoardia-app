const config = require("./jest.config");

module.exports = {
  ...config,
  testRegex: "src/domain/.*.spec.ts$",
};

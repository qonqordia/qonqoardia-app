import { ColorEnum } from "./../../../entities/color";

export type SetMyColorPort = {
  color: ColorEnum;
  token: string;
  userId: string;
};

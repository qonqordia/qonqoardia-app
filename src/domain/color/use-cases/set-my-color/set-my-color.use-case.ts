import { pipe } from 'fp-ts/function';
import { HttpColorRepository } from '../../../../infrastructure/http/http-color.repository';
import { SetMyColorErrors } from './set-my-color.errors';
import { TaskEither } from 'fp-ts/TaskEither';
import { SetMyColorPort } from "./set-my-color.port";
import { SetMyColorResult } from './set-my-color.result';
import { ColorRepository } from '../../gateways/color.repository';
import { Option } from 'fp-ts/Option';
import { option } from 'fp-ts';

export class SetMyColorUseCase {
  private static instance: Option<SetMyColorUseCase> = option.none;

  constructor(private readonly colorRepository: ColorRepository) {}

  public static getInstance(): SetMyColorUseCase {
    return pipe(
      SetMyColorUseCase.instance,
      option.fold(
        () => {
          const instance = new SetMyColorUseCase(
            HttpColorRepository.getInstance(),
          );
          SetMyColorUseCase.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public execute(port: SetMyColorPort): TaskEither<SetMyColorErrors, SetMyColorResult> {
    return this.colorRepository.setColorByUserId(port.userId, port.color, port.token);
  }
}

import { option } from 'fp-ts';
import { Option } from 'fp-ts/Option';
import { Color } from './../entities/color';
import { Observable } from "../../utils/Observable";

export type ColorState = {
  myColor: Observable<Option<Color>>;
};

export const colorState: ColorState = {
  myColor: new Observable<Option<Color>>(option.none),
};

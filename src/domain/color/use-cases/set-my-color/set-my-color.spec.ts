import { ColorEnum } from "./../../../entities/color";
import { Color } from "../../../entities/color";
import { toPromise } from "../../../../utils/functions";
import { InMemoryColorRepository } from "../../../../infrastructure/in-memory/in-memory-color.repository";
import { SetMyColorUseCase } from "./set-my-color.use-case";
import { SetMyColorResult } from "./set-my-color.result";
import { merge } from "lodash";

describe("set my color", () => {
  const id: string = "5784cf7b-1e8e-4bd5-84ac-f8b879147cbe";
  const userId: string = "ad836b58-0293-4421-a8f4-6b8bf33c2503";
  const color: Color = {
    id,
    color: "Green",
    userId,
  };
  const newColor: ColorEnum = "Red";
  const token: string = "4f4d847a-543b-4194-9702-d4a2b85e212a";

  let colorRepository: InMemoryColorRepository;
  let useCase: SetMyColorUseCase;

  let promise: Promise<SetMyColorResult>;

  beforeEach(() => {
    colorRepository = new InMemoryColorRepository([color]);
    useCase = new SetMyColorUseCase(colorRepository);

    promise = toPromise(useCase.execute({ color: newColor, token, userId }));
  });
  test("should succeed", async () => {
    await expect(promise).resolves.toBe(undefined);
  });
  test("should have changed color", async () => {
    await promise;

    expect(colorRepository.colors).toMatchObject([
      merge({}, color, {
        color: newColor,
      }),
    ]);
  });
});

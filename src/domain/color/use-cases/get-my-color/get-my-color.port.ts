import { Option } from 'fp-ts/Option';
import { Authenticator } from "../../../entities/authenticator";

export type GetMyColorPort = {
  authenticator: Option<Authenticator>
};

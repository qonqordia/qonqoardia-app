import { option } from "fp-ts";
import { authenticationState } from "../../authentication.store";
import { taskEither } from "fp-ts";
import { pipe, constVoid } from "fp-ts/function";
import { TaskEither } from "fp-ts/TaskEither";
import { HttpAuthenticationRepository } from "../../../../infrastructure/http/http-authentication.repository";
import { AuthenticationRepository } from "../../gateways/authentication.repository";
import { LoginErrors } from "./login.errors";
import { LoginPort } from "./login.port";
import { LoginResult } from "./login.result";
import { Option } from "fp-ts/Option";

class LoginUseCase {
  private static instance: Option<LoginUseCase> = option.none;

  constructor(
    private readonly authenticationRepository: AuthenticationRepository
  ) {
    this.authenticationRepository = authenticationRepository;
  }

  public static getInstance(): LoginUseCase {
    return pipe(
      LoginUseCase.instance,
      option.fold(
        () => {
          const instance = new LoginUseCase(
            HttpAuthenticationRepository.getInstance()
          );
          LoginUseCase.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public execute(port: LoginPort): TaskEither<LoginErrors, LoginResult> {
    return pipe(
      this.authenticationRepository.login(port.email, port.password),
      taskEither.map((authenticator) => {
        authenticationState.authenticator.set(option.some(authenticator));

        return constVoid();
      })
    );
  }
}

export { LoginUseCase };

export type Authenticator = {
  token: string;
  userId: string;
};

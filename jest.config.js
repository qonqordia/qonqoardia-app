module.exports = {
  coveragePathIgnorePatterns: ["/generated/", "node_modules"],
  globals: { "ts-jest": { isolatedModules: true } },
  // reporters: ["default", "jest-junit"],
  testEnvironment: "node",
  testTimeout: 15000,
  transform: { "^.+\\.ts$": "ts-jest" },
};

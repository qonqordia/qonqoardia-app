import { option } from "fp-ts";
import { InternalError } from "../../../../common/errors/internal.error";
import { InMemoryAuthenticationRepository } from "../../../../infrastructure/in-memory/in-memory-authentication.repository";
import { toPromise } from "../../../../utils/functions";
import { WrongCredentialsError } from "../../errors/wrong-credentials.error";
import { LoginResult } from "./login.result";
import { LoginUseCase } from "./login.use-case";

describe("login", () => {
  const email = "e2e075b5-c5ed-4456-a359-80971ffc481f@77745dfa-4dd1-432a-89e9-9e40138b0d89";
  const password = "b5a3b7ad-e56b-47cb-b48d-6feb5f795c24";

  let authenticationRepository: InMemoryAuthenticationRepository;
  let useCase: LoginUseCase;

  let promise: Promise<LoginResult>;

  describe("when api is up and running", () => {
    describe("when infos are correct", () => {
      beforeEach(() => {
        authenticationRepository = new InMemoryAuthenticationRepository([{
          email,
          password: option.some(password),
        }]);
        useCase = new LoginUseCase(authenticationRepository);
        promise = toPromise(useCase.execute({ email, password }));
      });
      test("should login", async () => {
        await expect(promise).resolves.toBe(undefined);
      });
    });
  
    describe("when email is wrong", () => {
      beforeEach(() => {
        promise = toPromise(useCase.execute({ email: "7006039c-dc81-4b58-bdc1-252658e0f267@542174ac-a7f6-402a-a15d-aa9c40041756", password }));
      });
      test("should handle error", async () => {
        await expect(promise).rejects.toThrow(WrongCredentialsError.CODE);
      });
    });
  
    describe("when password is wrong", () => {
      beforeEach(() => {
        promise = toPromise(useCase.execute({ email, password: "d8aa2ff4-f02c-48da-acc9-05a855f73811" }));
      });
      test("should handle error", async () => {
        await expect(promise).rejects.toThrow(WrongCredentialsError.CODE);
      });
    });
  });

  describe("when api is down", () => {
    beforeEach(() => {
      authenticationRepository = new InMemoryAuthenticationRepository([{
        email,
        password: option.none,
      }], true);
      useCase = new LoginUseCase(authenticationRepository);
      promise = toPromise(useCase.execute({ email, password }));
    });
    test("should handle error", async () => {
      await expect(promise).rejects.toThrow(InternalError.CODE);
    });
  });
});
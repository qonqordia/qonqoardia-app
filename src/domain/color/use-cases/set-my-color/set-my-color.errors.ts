import { InternalError } from '../../../../common/errors/internal.error';

export type SetMyColorErrors = InternalError;
import { Authenticator } from "./../../domain/entities/authenticator";
import { AuthenticationRepository } from "../../domain/authentication/gateways/authentication.repository";
import { pipe, constVoid } from "fp-ts/function";
import { option, taskEither } from "fp-ts";
import { TaskEither } from "fp-ts/TaskEither";
import { InternalError } from "../../common/errors/internal.error";
import { WrongCredentialsError } from "../../domain/authentication/errors/wrong-credentials.error";
import { EmailAlreadyUsedError } from "../../domain/authentication/errors/email-already-used.error";
import { Option } from "fp-ts/lib/Option";

export type Credentials = {
  email: string;
  password: Option<string>;
};

export class InMemoryAuthenticationRepository
  implements AuthenticationRepository
{
  private credentialsList: Credentials[];
  private isFailing: boolean;

  constructor(credentialsList: Credentials[] = [], isFailing: boolean = false) {
    this.credentialsList = credentialsList;
    this.isFailing = isFailing;
  }

  login(
    email: string,
    password: string
  ): TaskEither<InternalError | WrongCredentialsError, Authenticator> {
    if (this.isFailing) {
      return taskEither.left(new InternalError());
    }

    return pipe(
      taskEither.of(
        this.credentialsList.find(
          (credentials) =>
            credentials.email === email && option.toNullable(credentials.password) === password
        )
      ),
      taskEither.chainW(
        taskEither.fromPredicate(
          (foundCredentials) => !!foundCredentials,
          () => new WrongCredentialsError()
        )
      ),
      taskEither.map(() => ({
        token: `${email}${password}`,
        userId: email,
      }))
    );
  }

  register(
    email: string
  ): TaskEither<InternalError | EmailAlreadyUsedError, void> {
    if (this.isFailing) {
      return taskEither.left(new InternalError());
    }

    return pipe(
      taskEither.of(
        this.credentialsList.find((credentials) => credentials.email === email)
      ),
      taskEither.chainW(
        taskEither.fromPredicate(
          (credentials) => !credentials,
          () => new EmailAlreadyUsedError()
        )
      ),
      taskEither.map(constVoid)
    );
  }
}

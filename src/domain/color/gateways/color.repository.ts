import { Color, ColorEnum } from './../../entities/color';
import { TaskEither } from 'fp-ts/TaskEither';
import { InternalError } from '../../../common/errors/internal.error';

export interface ColorRepository {
  setColorByUserId(userId: string, color: ColorEnum, token: string): TaskEither<InternalError, void>;
  getColorByUserId(userId: string, token: string): TaskEither<InternalError, Color>;
}
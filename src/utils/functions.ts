import { TaskEither, tryCatch } from 'fp-ts/TaskEither';
import { taskEither } from 'fp-ts';
import { identity, pipe, Lazy } from "fp-ts/function";
import { InternalError } from '../common/errors/internal.error';

export async function toPromise<E, T>(
  input: TaskEither<E, T>,
): Promise<T> {
  return pipe(input, taskEither.map(identity), taskEither.getOrElse(error => { throw error }))();
}

export function toTaskEither<A>(
  f: Lazy<Promise<A>>,
): TaskEither<InternalError, A> {
  return tryCatch(f, originalError => new InternalError("INTERNAL", originalError as Error));
}

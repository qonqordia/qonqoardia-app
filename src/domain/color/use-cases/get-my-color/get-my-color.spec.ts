import { option } from "fp-ts";
import { Color } from "../../../entities/color";
import { toPromise } from "../../../../utils/functions";
import { InMemoryColorRepository } from "../../../../infrastructure/in-memory/in-memory-color.repository";
import { GetMyColorUseCase } from "./get-my-color.use-case";
import { GetMyColorResult } from "./get-my-color.result";

describe("get my color", () => {
  const userId: string = "3dc075d9-96ad-42b3-87bb-c4872f3ae232";
  const id: string = "5784cf7b-1e8e-4bd5-84ac-f8b879147cbe";
  const color: Color = {
    id,
    color: "Green",
    userId,
  };
  const authenticator = option.some({
    token: "a58aeac5-4f0a-4f3d-b4b9-91472997e84c",
    userId,
  });

  let colorRepository: InMemoryColorRepository;
  let useCase: GetMyColorUseCase;

  let promise: Promise<GetMyColorResult>;

  beforeEach(() => {
    colorRepository = new InMemoryColorRepository([color]);
    useCase = new GetMyColorUseCase(colorRepository);

    promise = toPromise(useCase.execute({ authenticator }));
  });
  test("should return my color", async () => {
    await expect(promise).resolves.toBe(color);
  });
});

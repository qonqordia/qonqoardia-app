import { NotAuthenticatedError } from './../../../../common/errors/not-authenticated.error';
import { InternalError } from '../../../../common/errors/internal.error';

export type GetMyColorErrors = InternalError | NotAuthenticatedError;
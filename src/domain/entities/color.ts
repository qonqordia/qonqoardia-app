export type ColorEnum = "Green" | "Orange" | "Red";

export type Color = {
  id: string;
  color: ColorEnum;
  userId: string;
}

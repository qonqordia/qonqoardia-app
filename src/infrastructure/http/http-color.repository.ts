import { Option } from "fp-ts/Option";
import { TaskEither } from "fp-ts/TaskEither";
import { ColorRepository } from "../../domain/color/gateways/color.repository";
import { option, taskEither } from "fp-ts";
import { constVoid, pipe } from "fp-ts/function";
import axios from "axios";
import { toTaskEither } from "../../utils/functions";
import { InternalError } from "../../common/errors/internal.error";
import { env } from "../env/env";
import { Color, ColorEnum } from "../../domain/entities/color";

export class HttpColorRepository implements ColorRepository {
  constructor(private readonly url: string) {}

  private static instance: Option<HttpColorRepository> = option.none;

  public static getInstance(): HttpColorRepository {
    return pipe(
      HttpColorRepository.instance,
      option.fold(
        () => {
          const instance = new HttpColorRepository(env().url);
          HttpColorRepository.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public setColorByUserId(
    userId: string,
    color: ColorEnum,
    token: string
  ): TaskEither<InternalError, void> {
    return pipe(
      toTaskEither(() =>
        axios.create({ baseURL: this.url }).patch(`/users/${userId}/colors`, {
          color,
        })
      ),
      taskEither.map(() => constVoid())
    );
  }

  public getColorByUserId(userId: string, token: string): TaskEither<InternalError, Color> {
    return pipe(
      toTaskEither(() => axios.create({ baseURL: this.url }).get(`/users/${userId}/colors`)),
      taskEither.map((response) => response.data)
    );
  }
}

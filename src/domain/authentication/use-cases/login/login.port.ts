type LoginPort = {
  email: string;
  password: string;
}

export type { LoginPort };

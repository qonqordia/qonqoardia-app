import { NotFoundError } from "./../../common/errors/not-found.error";
import { pipe } from "fp-ts/function";
import { taskEither, option } from "fp-ts";
import { constVoid } from "fp-ts/function";
import { TaskEither } from "fp-ts/TaskEither";
import { Color, ColorEnum } from "../../domain/entities/color";
import { ColorRepository } from "./../../domain/color/gateways/color.repository";

export class InMemoryColorRepository implements ColorRepository {
  constructor(public colors: Color[] = []) {}

  setColorByUserId(
    userId: string,
    newColor: ColorEnum
  ): TaskEither<never, void> {
    const foundColorIndex = this.colors.findIndex(
      (color) => color.userId === userId
    );

    if (foundColorIndex > -1) {
      this.colors[foundColorIndex].color = newColor;
    }

    return taskEither.of(constVoid());
  }

  getColorByUserId(userId: string): TaskEither<NotFoundError, Color> {
    return pipe(
      this.colors.find((color) => color.userId === userId),
      option.fromNullable,
      option.fold(() => taskEither.left(new NotFoundError()), taskEither.of)
    );
  }
}

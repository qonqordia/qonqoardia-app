import { Color } from './../../../entities/color';

export type GetMyColorResult = Color;
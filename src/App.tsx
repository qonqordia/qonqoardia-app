import {
  setupIonicReact
} from '@ionic/react';
import '@ionic/react/css/core.css';
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

import './theme/variables.css';

// import MainPage from './presentation/pages/MainPage';
import { FunctionComponent } from 'react';
import WelcomePage from './presentation/pages/WelcomePage';

setupIonicReact();

const App: FunctionComponent<{
}> = () => (<WelcomePage />);

export default App;

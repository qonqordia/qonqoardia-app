export class Observable<A> {
  constructor(
    private value: A,
    private listeners: Array<(a: A) => void> = []
  ) {}

  public set(value: A): void {
    this.value = value;
    this.listeners.forEach((listener) => listener(value));
  }

  public get(): A {
    return this.value;
  }

  public subscribe<B extends (a: A) => void>(listener: B): () => void {
    this.listeners.push(listener);
    return () => this.unsubscribe(listener);
  }

  public unsubscribe(listener: (a: A) => void): void {
    this.listeners = this.listeners.filter((l) => l !== listener);
  }
}
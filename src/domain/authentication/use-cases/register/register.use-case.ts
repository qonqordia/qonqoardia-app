import { pipe } from "fp-ts/function";
import { option } from "fp-ts";
import { Option } from "fp-ts/Option";
import { TaskEither } from "fp-ts/TaskEither";
import { HttpAuthenticationRepository } from "../../../../infrastructure/http/http-authentication.repository";
import { AuthenticationRepository } from "../../gateways/authentication.repository";
import { RegisterErrors } from "./register.errors";
import { RegisterPort } from "./register.port";
import { RegisterResult } from "./register.result";

class RegisterUseCase {
  private static instance: Option<RegisterUseCase> = option.none;

  constructor(
    private readonly authenticationRepository: AuthenticationRepository
  ) {
    this.authenticationRepository = authenticationRepository;
  }

  public static getInstance(): RegisterUseCase {
    return pipe(
      RegisterUseCase.instance,
      option.fold(
        () => {
          const instance = new RegisterUseCase(
            HttpAuthenticationRepository.getInstance()
          );
          RegisterUseCase.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public execute(
    port: RegisterPort
  ): TaskEither<RegisterErrors, RegisterResult> {
    return this.authenticationRepository.register(port.email);
  }
}

export { RegisterUseCase };

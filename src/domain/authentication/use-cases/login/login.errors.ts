import { InternalError } from "../../../../common/errors/internal.error";
import { WrongCredentialsError } from "../../errors/wrong-credentials.error";

type LoginErrors = InternalError | WrongCredentialsError;

export type { LoginErrors }
export class EmailAlreadyUsedError extends Error {
  public static readonly CODE = "EMAIL_ALREADY_USED";

  public originalMessage?: string;

  constructor(message?: string, options?: ErrorOptions) {
    super(EmailAlreadyUsedError.CODE, options);
    this.originalMessage = message;
  }
}

import { InternalError } from "../../../../common/errors/internal.error";
import { WrongCredentialsError } from "../../errors/wrong-credentials.error";

type RegisterErrors = InternalError | WrongCredentialsError;

export type { RegisterErrors }
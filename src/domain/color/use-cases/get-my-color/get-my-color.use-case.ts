import { taskEither } from 'fp-ts';
import { NotAuthenticatedError } from './../../../../common/errors/not-authenticated.error';
import { pipe } from 'fp-ts/function';
import { option } from 'fp-ts';
import { Option } from 'fp-ts/Option';
import { HttpColorRepository } from '../../../../infrastructure/http/http-color.repository';
import { GetMyColorErrors } from './get-my-color.errors';
import { TaskEither } from 'fp-ts/TaskEither';
import { GetMyColorPort } from "./get-my-color.port";
import { GetMyColorResult } from './get-my-color.result';
import { ColorRepository } from '../../gateways/color.repository';

export class GetMyColorUseCase {
  private static instance: Option<GetMyColorUseCase> = option.none;

  constructor(private readonly colorRepository: ColorRepository) {}

  public static getInstance(): GetMyColorUseCase {
    return pipe(
      GetMyColorUseCase.instance,
      option.fold(
        () => {
          const instance = new GetMyColorUseCase(
            HttpColorRepository.getInstance()
          );
          GetMyColorUseCase.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public execute(port: GetMyColorPort): TaskEither<GetMyColorErrors, GetMyColorResult> {
    return pipe(
      port.authenticator,
      option.fold(
        () => taskEither.left(new NotAuthenticatedError()),
        (authenticator) => this.colorRepository.getColorByUserId(authenticator.userId, authenticator.token)
      )
    );
  }
}

export class WrongCredentialsError extends Error {
  public static readonly CODE = "WRONG_CREDENTIALS";

  public originalMessage?: string;

  constructor(message?: string, options?: ErrorOptions) {
    super(WrongCredentialsError.CODE, options);
    this.originalMessage = message;
  }
};

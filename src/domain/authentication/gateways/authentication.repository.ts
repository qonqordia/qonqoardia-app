import { Authenticator } from './../../entities/authenticator';
import { TaskEither } from 'fp-ts/TaskEither';
import { InternalError } from '../../../common/errors/internal.error';
import { EmailAlreadyUsedError } from '../errors/email-already-used.error';
import { WrongCredentialsError } from '../errors/wrong-credentials.error';

interface AuthenticationRepository {
  login(email: string, password: string): TaskEither<InternalError | WrongCredentialsError, Authenticator>;
  register(email: string): TaskEither<InternalError | EmailAlreadyUsedError, void>;
}

export type { AuthenticationRepository };
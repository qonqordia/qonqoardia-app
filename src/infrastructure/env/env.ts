export const env = () => ({
  url: "http://127.0.0.1:3000",
});

export type Env = ReturnType<typeof env>;

export class NotFoundError extends Error {
  public static readonly CODE = "NOT_FOUND";

  public originalMessage?: string;

  constructor(message?: string, options?: ErrorOptions) {
    super(NotFoundError.CODE, options);
    this.originalMessage = message;
  }
}

import {
  IonApp,
  IonCol,
  IonHeader,
  IonIcon,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { option, taskEither } from "fp-ts";
import { constVoid, pipe } from "fp-ts/function";
import { Option } from "fp-ts/Option";
import { radioButtonOffOutline, radioButtonOnOutline } from "ionicons/icons";
import { FunctionComponent, useEffect, useState } from "react";
import { authenticationState } from "../../domain/authentication/authentication.store";
import { colorState } from "../../domain/color/color.store";
import { GetMyColorUseCase } from "../../domain/color/use-cases/get-my-color/get-my-color.use-case";
import { SetMyColorUseCase } from "../../domain/color/use-cases/set-my-color/set-my-color.use-case";
import { Color } from "../../domain/entities/color";
import { toPromise } from "../../utils/functions";

type DashboardProps = {
  setMyColorUseCase: SetMyColorUseCase;
  getMyColorUseCase: GetMyColorUseCase;
};

const Dashboard: FunctionComponent<DashboardProps> = (props) => {
  const [myColor, setMyColor] = useState<Option<Color>>(option.none);

  useEffect(() => {
    async function getMyColor() {
      return pipe(
        props.getMyColorUseCase.execute({ authenticator: authenticationState.authenticator.get() }),
        taskEither.map((color) => {
          colorState.myColor.set(option.some(color));
  
          return constVoid();
        }),
        toPromise,
      )
    }
    colorState.myColor.subscribe((color) => {
      setMyColor(color);
    });

    getMyColor();
  });

  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Dashboard</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonRow>
        <IonCol>
          {option.toNullable(myColor)?.color === "Red" ? (
            <IonIcon
              style={{ fontSize: "70px", color: "#FF0000" }}
              icon={radioButtonOnOutline}
            />
          ) : (
            <IonIcon
              style={{ fontSize: "70px", color: "#000000" }}
              icon={radioButtonOffOutline}
            />
          )}
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          {option.toNullable(myColor)?.color === "Orange" ? (
            <IonIcon
              style={{ fontSize: "70px", color: "#FFFF00" }}
              icon={radioButtonOnOutline}
            />
          ) : (
            <IonIcon
              style={{ fontSize: "70px", color: "#000000" }}
              icon={radioButtonOffOutline}
            />
          )}
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          {option.toNullable(myColor)?.color === "Green" ? (
            <IonIcon
              style={{ fontSize: "70px", color: "#00FF00" }}
              icon={radioButtonOnOutline}
            />
          ) : (
            <IonIcon
              style={{ fontSize: "70px", color: "#000000" }}
              icon={radioButtonOffOutline}
            />
          )}
        </IonCol>
      </IonRow>
    </IonApp>
  );
};

export default Dashboard;

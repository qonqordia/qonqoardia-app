import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'qonqordia-app',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;

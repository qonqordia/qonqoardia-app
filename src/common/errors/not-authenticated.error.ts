export class NotAuthenticatedError extends Error {
  public static readonly CODE = "NOT_AUTHENTICATED";

  public originalMessage?: string;

  constructor(message?: string, options?: ErrorOptions) {
    super(NotAuthenticatedError.CODE, options);
    this.originalMessage = message;
  }
}

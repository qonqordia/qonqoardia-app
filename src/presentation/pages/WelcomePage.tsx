import { FunctionComponent, useEffect, useState } from "react";
import { Route } from "react-router-dom";
import {
  IonApp,
  IonTabBar,
  IonTabs,
  IonTabButton,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonContent,
} from "@ionic/react";
import Login from "../components/Login";
import Register from "../components/Register";
import { IonReactRouter } from "@ionic/react-router";
import { barChartOutline, logIn, personAdd, settingsOutline } from "ionicons/icons";
import { LoginUseCase } from "../../domain/authentication/use-cases/login/login.use-case";
import { RegisterUseCase } from "../../domain/authentication/use-cases/register/register.use-case";
import { Authenticator } from "../../domain/entities/authenticator";
import Dashboard from "../components/Dashboard";
import { authenticationState } from "../../domain/authentication/authentication.store";
import { SetMyColorUseCase } from "../../domain/color/use-cases/set-my-color/set-my-color.use-case";
import { GetMyColorUseCase } from "../../domain/color/use-cases/get-my-color/get-my-color.use-case";
import { option } from "fp-ts";
import { Option } from "fp-ts/Option";

const WelcomePage: FunctionComponent<{}> = () => {
  let [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    authenticationState.authenticator.subscribe((authenticator: Option<Authenticator>) => {
      setIsAuthenticated(option.isSome(authenticator));
    });
  });

  if (isAuthenticated) {
    return (
      <IonApp>
        <IonContent>
          <p>Welcome !</p>
          <IonReactRouter>
            <IonTabs>
              <IonRouterOutlet>
                <Route exact path="/dashboard">
                  <Dashboard
                    setMyColorUseCase={SetMyColorUseCase.getInstance()}
                    getMyColorUseCase={GetMyColorUseCase.getInstance()}
                  />
                </Route>
              </IonRouterOutlet>
              <IonTabBar slot="bottom">
                <IonTabButton tab="dashboard" href="/dashboard">
                  <IonIcon icon={barChartOutline}></IonIcon>
                  <IonLabel>Dashboard</IonLabel>
                </IonTabButton>
              </IonTabBar>
              <IonTabBar slot="bottom">
                <IonTabButton tab="parameters" href="/parameters">
                  <IonIcon icon={settingsOutline}></IonIcon>
                  <IonLabel>Parameters</IonLabel>
                </IonTabButton>
              </IonTabBar>
            </IonTabs>
          </IonReactRouter>
        </IonContent>
      </IonApp>
    )
  } else {
    return (
      <IonApp>
        <IonContent>
          <p>Welcome !</p>
          <IonReactRouter>
            <IonTabs>
              <IonRouterOutlet>
                <Route exact path="/register">
                  <Register registerUseCase={RegisterUseCase.getInstance()} />
                </Route>
                <Route exact path="/login">
                  <Login loginUseCase={LoginUseCase.getInstance()} />
                </Route>
              </IonRouterOutlet>
              <IonTabBar slot="bottom">
                <IonTabButton tab="login" href="/login">
                  <IonIcon icon={logIn}></IonIcon>
                  <IonLabel>Login</IonLabel>
                </IonTabButton>
                <IonTabButton tab="register" href="/register">
                  <IonIcon icon={personAdd}></IonIcon>
                  <IonLabel>Register</IonLabel>
                </IonTabButton>
              </IonTabBar>
            </IonTabs>
          </IonReactRouter>
        </IonContent>
      </IonApp>
    )
  }
};

export default WelcomePage;

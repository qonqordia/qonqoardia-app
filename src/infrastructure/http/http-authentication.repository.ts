import { Option } from 'fp-ts/Option';
import { option, taskEither } from "fp-ts";
import { pipe } from "fp-ts/function";
import { Authenticator } from "./../../domain/entities/authenticator";
import { TaskEither } from "fp-ts/TaskEither";
import { WrongCredentialsError } from "../../domain/authentication/errors/wrong-credentials.error";
import { AuthenticationRepository } from "../../domain/authentication/gateways/authentication.repository";
import axios from "axios";
import { toTaskEither } from "../../utils/functions";
import { InternalError } from "../../common/errors/internal.error";
import { EmailAlreadyUsedError } from "../../domain/authentication/errors/email-already-used.error";
import { env } from "../env/env";

export class HttpAuthenticationRepository implements AuthenticationRepository {
  constructor(private readonly url: string) {}

  private static instance: Option<HttpAuthenticationRepository> = option.none;

  public static getInstance(): HttpAuthenticationRepository {
    return pipe(
      HttpAuthenticationRepository.instance,
      option.fold(
        () => {
          const instance = new HttpAuthenticationRepository(
            env().url
          );
          HttpAuthenticationRepository.instance = option.some(instance);
          return instance;
        },
        (instance) => instance
      )
    );
  }

  public login(
    email: string,
    password: string
  ): TaskEither<InternalError | WrongCredentialsError, Authenticator> {
    return pipe(
      toTaskEither(() =>
        axios.create({ baseURL: this.url }).post("/login", { email, password })
      ),
      taskEither.map((response): Authenticator => response.data),
    );
  }

  public register(
    email: string
  ): TaskEither<InternalError | EmailAlreadyUsedError, void> {
    return toTaskEither(() =>
      axios.create({ baseURL: this.url }).post("/register", { email })
    );
  }
}

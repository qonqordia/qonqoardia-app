import { FunctionComponent } from "react";
import {
  IonApp,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonCol,
  IonRow,
  IonIcon,
  IonItem,
  IonLabel,
  IonInput,
  IonButton,
  useIonAlert,
} from "@ionic/react";
import { logIn } from "ionicons/icons";
import { LoginUseCase } from "../../domain/authentication/use-cases/login/login.use-case";
import { toPromise } from "../../utils/functions";
import { constVoid, pipe } from "fp-ts/function";
import { taskEither } from "fp-ts";
import { useHistory } from "react-router-dom";

export interface LoginProps {
  email?: string;
  password?: string;
  loginUseCase: LoginUseCase;
}

export interface LoginState {
  email: string;
  password: string;
  error?: Error;
}

const Login: FunctionComponent<LoginProps> = (props: LoginProps) => {
  const state: LoginState = {
    email: props.email ?? "",
    password: props.password ?? "",
  };
  const setEmail = (email: string): void => {
    state.email = email;
  };
  const setPassword = (password: string): void => {
    state.password = password;
  };
  const [presentAlert] = useIonAlert();
  const history = useHistory();
  const login = async (): Promise<void> => {
    return pipe(
      props.loginUseCase.execute({
        email: state.email ?? "",
        password: state.password ?? "",
      }),
      taskEither.chainFirstW(() => {
        history.push("/dashboard");

        return taskEither.of(constVoid())
      }),
      taskEither.mapLeft((error) => {
        presentAlert({
          header: "Error",
          subHeader: "An error occurred",
          message: error.message,
          buttons: ["OK"],
        });
        return error;
      }),
      toPromise
    ).catch(() => Promise.resolve());
  };
  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonRow>
        <IonCol>
          <IonIcon
            style={{ fontSize: "70px", color: "#0040ff" }}
            icon={logIn}
          />
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonItem>
            <IonLabel position="floating">Email</IonLabel>
            <IonInput
              type="email"
              required
              value={state.email}
              onIonChange={(e) => setEmail(e.detail.value!)}
            ></IonInput>
          </IonItem>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonItem>
            <IonLabel position="floating">Password</IonLabel>
            <IonInput
              type="password"
              required
              value={state.password}
              onIonChange={(e) => setPassword(e.detail.value!)}
            ></IonInput>
          </IonItem>
        </IonCol>
      </IonRow>
      <IonRow>
        <IonCol>
          <IonButton expand="block" onClick={login}>
            Login
          </IonButton>
        </IonCol>
      </IonRow>
    </IonApp>
  );
};

export default Login;
